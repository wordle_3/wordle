import requests, json
from random import choice
import filter #algo filename

header = {'Content-Type': 'application/json'}

def register_player(session, url, player_name):
    response = session.post(f'{url}/register', json={"name": player_name}, headers=header)
    return response.json()['id']

def create_game(session, url, player_id):
    session.post(f'{url}/create', json={"id": player_id, "overwrite": True}, headers=header)

def make_guess(session, url, player_id, guess):
    response = session.post(f'{url}/guess', json={'id': player_id, 'guess': guess}, headers=header)
    return response.json()

session = requests.session()
base_url = 'https://we6.talentsprint.com/game'
player_name = 'annapurna'
possibles_file = 'sowpods_all.txt'

player_id = register_player(session, base_url, player_name)
create_game(session, base_url, player_id)

data = filter.load_data(possibles_file)
current_guess = choice(data)
current_feedback = ''

count = 0
while  count != 6 or current_feedback != 'GGGGG':
    response = make_guess(session, base_url, player_id, current_guess)
    current_feedback = response['feedback']
    count += 1
    print(f"Current Guess: {current_guess}, Feedback: {current_feedback}")
    if current_feedback == 'GGGGG': 
        print(f'Victory!')
        exit()
    # else:
    #     print('Defeat!')
    data = filter.select(data, current_guess, current_feedback)
    if len(data) != 0:
        current_guess = choice(data)
    elif current_feedback == None:
        print("Word not in possible words :()")
        exit()
print("Defeat!, Game is over!")


