import random
def load_data(filename: str) -> list[str]:
    pos = []
    with open(filename) as f:
        for line in f:
            if len(line.strip()) == 5:
                pos.append(line.strip())
    return pos

def rfilter(data: list[str], ch: str) -> list[str]:
    copy = [i for i in data if ch not in i]
    return copy

def yfilter(data: list[str], ch: str, pos: int) -> list[str]:
    copy = [i for i in data if i[pos] != ch and ch in i]
    return copy

def gfilter(data: list[str], ch: str, pos: int) -> list[str]:
    copy = [i for i in data if i[pos] == ch]
    return copy

def select(data: list[str], guess_word: str, feedback: str) -> str:
    for index, e in enumerate(zip(guess_word, feedback)):
        if e[1] == 'G':
            data = gfilter(data, e[0], index)     
        elif e[1] == 'Y':
            data = yfilter(data, e[0], index)
        else:
            data = rfilter(data, e[0])
        # if e[1] == 'R':
        #     data = rfilter(data, e[0])
        # elif e[1] == 'G':
        #     data = gfilter(data, e[0], index)     
        # elif e[1] == 'Y':
        #     data = yfilter(data, e[0], index)     
     
        # print(data)
    return data
