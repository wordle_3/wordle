# WORDLE
-------

## Games Rules
-------------

1. The player has six attempts to guess a five-letter word.

2. After each guess, feedback is provided:
	* G -> (stands for Green) Correct letter in the correct position.
	* Y ->(stands for Yellow) Correct letter in the wrong position.
	* R ->(stands for Red) Incorrect letter.

3. The game ends when the player guesses the word correctly or uses all six attempts.


## Algorithm Overview
--------------------
1. Create a txt file of all valid five-letters words.

2. Create a python file that handles JSON data, allowing easy interaction between JSON files and Python code.

2. Randomly select a target word.

3. Loop until we get 'GGGGG' or None as feedback:
	- Get and validate player's guess.
	- Provide feedback on the guess.

4. End the game with a win or defeat!


